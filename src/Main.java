import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

class Segment implements Comparable<Segment> {
    private int idx;
    private int numTanks;
    private int length;

    Segment(int idx, int length, int numTanks) {
        this.idx = idx;
        this.numTanks = numTanks;
        this.length = length;
    }

    @Override
    public int compareTo(Segment segment) {
        return (this.length < segment.length) ? 1 : (this.length > segment.length) ? -1 : 0;
    }

    @Override
    public String toString() {
        return "( " + idx + ", " + length + ", "  + numTanks + ") ";
    }

    public int getNumTanks() {
        return numTanks;
    }

    public void setNumTanks(int numTanks) {
        this.numTanks = numTanks;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public int getLenght() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}

public class Main {
    /**
     * @param coords  coordinates sorted by asc
     * @param countTanks
     * @return
     */
    protected static int[] calculate( int[] coords, int countTanks ) {
        final int countCities = coords.length;

        PriorityQueue<Segment> segments = new PriorityQueue<>(countCities-1);

        for (int i=1, k=countCities ; i<k ; i++) {
            segments.offer( new Segment(i-1, coords[i]-coords[i-1], 0));
        }

        for (int i=0, k=countTanks; i<k; i++) {
            final Segment maxSegment = segments.poll();
            final int currentTanks = maxSegment.getNumTanks();
            final int currentLenght = maxSegment.getLenght();

            maxSegment.setLength(  currentLenght * (currentTanks + 1)  / (currentTanks + 2));
            maxSegment.setNumTanks(currentTanks + 1);
            segments.offer(maxSegment);
        }

        int[] tanks = new int[countCities-1];

        for (int i=0, k=countCities-1 ; i<k ; i++) {
            Segment maxSegment = segments.poll();
            tanks[maxSegment.getIdx()] = maxSegment.getNumTanks();
        }

        return tanks;
    }


    public static void main(String[] args) throws Exception {
        int[] coords;

        //coords = new int[]{ 100, 150, 180, 250, 295, 425, 500, 510, 560, 600, 700, 750, 770, 1100 };

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = br.readLine();
        int numCities = Integer.parseInt(line);
        line = br.readLine();
        int numTanks = Integer.parseInt(line);

        Set<Integer> rawCoords = new HashSet<>();
        while (rawCoords.size() < numCities) {
            line = br.readLine();
            int cityCoord = Integer.parseInt(line);
            rawCoords.add(cityCoord);
        }
        //фильтровать повторяющиеся координаты

        coords = new int[numCities];
        int i=0;
        for(Integer c: rawCoords) {
            coords[i++] = c;
        }
        Arrays.sort(coords);

        System.out.println(Arrays.toString(calculate(coords, numTanks)));
    }
}
